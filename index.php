<!DOCTYPE html>
<?php
session_start();
?>
<html lang="en">
<!-- Website template from http://freemiumdownload.com/demo?theme=bootstrap-red-restaurant -->
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta name="description" content="Free Bootstrap Themes designed by Zerotheme.com" />
	<meta name="author" content="www.Zerotheme.com" />
	<link rel="icon" href="images/logo_new.png"/>
	<title>Pizza Man</title>

	<!-- Bootstrap Core CSS -->
	<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
	<link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>

	<!-- Custom Theme files -->
	<link href="css/style.css" rel="stylesheet" type="text/css"/>
	<link href="css/popuo-box.css" rel="stylesheet" type="text/css" media="all"/>
	<link href="css/contact-buttons.css" rel="stylesheet"/>

	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script src="js/jquery.min.js"></script>

	<!---pop-up-box---->
	<script type="text/javascript" src="js/modernizr.custom.min.js"></script>
	<script src="js/jquery.magnific-popup.js" type="text/javascript"></script>

	<!---//pop-up-box--->
	<script>
		$(document).ready(function() {
			$('.popup-with-zoom-anim').magnificPopup({
				type: 'inline',
				fixedContentPos: false,
				fixedBgPos: true,
				overflowY: 'auto',
				closeBtnInside: true,
				preloader: false,
				midClick: true,
				removalDelay: 300,
				mainClass: 'my-mfp-zoom-in'
			});
		});
	</script>

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
    <![endif]-->
</head>

<body>
	<!-- Modal templates from https://www.w3schools.com/bootstrap/bootstrap_ref_js_modal.asp -->
	<!-- Login Modal -->
	<div class="modal fade" id="loginModal" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header" style="padding:35px 50px;">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4><i class="fa fa-lock"></i> Login</h4>
				</div>

				<div class="modal-body" style="padding:40px 50px;">
					<form role="form" id="login" method="POST">
						<div class="form-group">
							<label for="usrname"><i class="fa fa-user"></i> Email Address</label>
							<input type="email" name="email" class="form-control" id="emailLogin" placeholder="Enter email" required="required">
						</div>
						<div class="form-group">
							<label for="psw"><i class="fa fa-key"></i> Password</label>
							<input type="password" name="password" class="form-control" id="passwordLogin" placeholder="Enter password" required="required">
						</div>
						<!-- <div class="checkbox">
						  <label><input type="checkbox" value="" checked>Remember me</label>
						</div> -->
						<button type="submit" class="btn btn-danger btn-block"><i class="fas fa-sign-in-alt"></i>Login</button>
					</form>

					<script type="text/javascript">
						var form = document.getElementById("login");
						form.addEventListener("submit", function (event) {
							event.preventDefault();
							login();
						});
						function login() {
							var data = new FormData();
							var email = document.getElementById("emailLogin").value;
							var password = document.getElementById("passwordLogin").value;
							data.append('email', email);
							data.append('password', password);
							var xhttp  = new XMLHttpRequest();
							xhttp.onreadystatechange = function() {
								if (this.readyState == 4 && this.status == 200) {
									if(this.responseText=="Login Successful"){
										window.alert("Login Successful");
										location.reload();
									} else if (this.responseText=="Invalid Password") {
										window.alert("Invalid Password");
									} else if (this.responseText=="Invalid Email"){
										window.alert("Invalid Email");
									}
								}
							};
							xhttp.open("POST", "login.php", true);
							xhttp.send(data);
						}
					</script>

				</div>

				<div class="modal-footer">
					<button type="submit" class="btn btn-danger btn-default pull-left" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancel</button>
					<p>Not a member? <button type="button" class="btn signupbtn" id="myBtn2" name="myBtn2" data-toggle="#signModal">Sign Up</button></p>
					<button type="button" class="btn signupbtn" id="myBtn3" data-toggle="#resetModal">Forgot your password?</button>
				</div>
			</div>
		</div>
	</div>

	<!-- Sign up modal -->
	<div class="modal fade" id="signModal" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header" style="padding:35px 50px;">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4><i class="fa fa-lock"></i> Sign Up</h4>
				</div>

				<div class="modal-body" style="padding:40px 50px;">
					<form role="form" id="signup" method="POST">
						<div class="form-group">
							<label for="fname"><i class="fa fa-user"></i> First Name</label>
							<input type="text" class="form-control" id="fnameSignup" name="first_name" placeholder="Enter First Name" required="required">
						</div>
						<div class="form-group">
							<label for="lname"><i class="fa fa-user"></i> Last Name</label>
							<input type="text" class="form-control" id="lnameSignup" name="last_name" placeholder="Enter Last Name" required="required">
						</div>
						<div class="form-group">
							<label for="email"><i class="fa fa-envelope"></i> Email Address</label>
							<input type="email" class="form-control" id="emailSignup" name="email" placeholder="Enter email" required="required">
						</div>
						<div class="form-group">
							<label for="psw"><i class="fa fa-key"></i> Password</label>
							<input type="password" class="form-control" id="passwordSignup" name="password" placeholder="Enter password" required="required">
						</div>
						<div class="form-group">
							<label for="address"><i class="fa fa-map-marker"></i> Home Address</label>
							<input type="text" class="form-control" id="addressSignup" name="home_address" placeholder="Enter Address">
						</div>
					<!-- <div class="checkbox">
					  <label><input type="checkbox" value="" checked>Remember me</label>
					</div> -->
					<button type="submit" class="btn btn-danger btn-block"><i class="fas fa-sign-in-alt"></i> Create Account</button>
				</form>

				<script type="text/javascript">
					var form = document.getElementById("signup");
					form.addEventListener("submit", function (event) {
						event.preventDefault();
						signup();
					});
					function signup() {
						var data = new FormData();
						var fname = document.getElementById("fnameSignup").value;
						var lname = document.getElementById("lnameSignup").value;
						var email = document.getElementById("emailSignup").value;
						var password = document.getElementById("passwordSignup").value;
						var address = document.getElementById("addressSignup").value;
						data.append('first_name', fname);
						data.append('last_name', lname);
						data.append('email', email);
						data.append('password', password);
						data.append('home_address', address);
						var xhttp  = new XMLHttpRequest();
						xhttp.onreadystatechange = function() {
							if (this.readyState == 4 && this.status == 200) {
								if(this.responseText=="Records added successfully."){
									window.alert("Sign Up Successful");
									jQuery("#signModal").modal('hide');
								} else if (this.responseText=="Email already exists") {
									window.alert("Email already exists");
								} else {
									window.alert(this.responseText);
								}
							}
						};
						xhttp.open("POST", "signup.php", true);
						xhttp.send(data);
					}
				</script>

				</div>
				<div class="modal-footer">
				<button type="submit" class="btn btn-danger btn-default pull-left" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancel</button>
				</div>
			</div>
		</div>
	</div>

	<!-- Forgot password Modal -->
	<div class="modal fade" id="resetModal" role="dialog">
		<div class="modal-dialog">
			
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header" style="padding:35px 50px;">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4><i class="fa fa-lock"></i> Reset Password</h4>
				</div>
				
				<div class="modal-body" style="padding:40px 50px;">
					<p>Enter your email address and an email will be sent to reset your password.</p>
					<form role="form" action="forgot_password.php" method="POST">
						<div class="form-group">
							<label for="usrname"><i class="fa fa-envelope"></i> Email Address</label>
							<input name="email" class="form-control" id="emailReset" placeholder="Enter email" required="required">
						</div>
						<button type="submit" class="btn btn-danger btn-block"><i class="fas fa-sign-in-alt"></i> Reset Password</button>
					</form>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-danger btn-default pull-left" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancel</button>
				</div>
			</div>
		</div>
	</div>

<!--Account Modal-->
<div class="modal fade" id="accountModal" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header" style="padding:35px 50px;">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4><i class="fa fa-lock"></i>Account</h4>
			</div>
			<div class="modal-body" style="padding:40px 50px;">
				<button onclick="logout()" type="button" class="btn btn-danger" id="logoutBtn"><i class="fa fa-user fa-fw"></i>Logout</button>
				<button type="button" class="btn btn-primary" id="accBtn" data-toggle="modal" data-target="#accountModal2"><i class="fa fa-user fa-fw"></i>Modify Account</button>
			</div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-danger btn-default pull-left" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancel</button>
			</div>
			<script type="text/javascript">
				
				
				function logout(){
					var xmlhttp = new XMLHttpRequest();
					xmlhttp.onreadystatechange = function() {
							if (this.readyState == 4 && this.status == 200) {
								window.alert("Logged out");
								location.reload();
							}
						};
					xmlhttp.open("GET", "unset.php", true);
					xmlhttp.send();
				}
			</script>
		</div>
	</div>
</div>

<!--Account Modal 2-->
<div class="modal fade" id="accountModal2" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header" style="padding:35px 50px;">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4><i class="fa fa-lock"></i>Modify</h4>
			</div>
			<div class="modal-body" style="padding:40px 50px;">
				<form role="form" id="change">
					<div class="form-group">
						<label for="newPassword"><i class="fa fa-envelope"></i>New Password</label>
						<input name="password" class="form-control" id="passwordNew" placeholder="Enter new password" required="required">
					</div>
					<button type="submit" class="btn btn-danger btn-block"><i class="fas fa-sign-in-alt"></i>Update</button>
				</form>
			</div>
			<div class="modal-body" style="padding:40px 50px;">
				<button onclick="deleteAccount()" type="button" class="btn btn-danger btn-block" id="deleteBtn"><i class="fa fa-user fa-fw"></i>Delete Account</button>
			</div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-danger" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancel</button>
			</div>
			<script type="text/javascript">
				var form = document.getElementById("change");
					form.addEventListener("submit", function (event) {
						event.preventDefault();
						change();
					});
				function change(){
					var data = new FormData();
					var newpass = document.getElementById("passwordNew").value;
					data.append('email', email);
					data.append('password', newpass);
					var xmlhttp = new XMLHttpRequest();
					xmlhttp.onreadystatechange = function() {
						if (this.readyState == 4 && this.status == 200) {
							if (this.responseText=="Password changed") {
								window.alert("Password changed");
								jQuery("#accountModal2").modal('hide');
							} else {
								window.alert(this.responseText);
							}
						}
					};
					xmlhttp.open("POST", "changepass.php", true);
					xmlhttp.send(data);
				}
				
				function deleteAccount(){
					var data = new FormData();
					data.append('email', email);
					var xmlhttp = new XMLHttpRequest();
					xmlhttp.onreadystatechange = function() {
						if (this.readyState == 4 && this.status == 200) {
							if (this.responseText=="Account deleted") {
								logout();
								window.alert("Account deleted");
								location.reload();
							} else {
								window.alert(this.responseText);
							}
						}
					};
					xmlhttp.open("POST", "deleteAccount.php", true);
					xmlhttp.send(data);
				}

			</script>
		</div>
	</div>
</div>

	<!-- Navigation -->
	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
				data-target="#bs-example-navbar-collapse-1">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>

			</button>
			<a class="navbar-brand" href="index.php">
				<img src="images/logo_new.png" class="hidden-xs" alt="Logo">
				<h3 class="visible-xs">Pizza Man</h3>
			</a>

		</div>
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav navbar-right">
				<li class="nav_links">
					<a class="page-scroll" href="menu.php"><i class="fa fa-cutlery fa-fw"></i> Menu</a>
				</li>
				<li class="nav_links">
					<a class="page-scroll" href="index.php#location"><i class="fa fa-map-marker fa-fw"></i> Store Location</a>
				</li>
				<li class="nav_links">
					<a class="page-scroll" href="index.php#booking"><i class="fa fa-book fa-fw"></i> Booking</a>
				</li>
				<li class="nav_links">
					<a class="page-scroll" href="index.php#contact"><i class="fa fa-phone fa-fw"></i> Contact Us</a>
				</li>
					<?php
						if (isset($_SESSION['email']) && isset($_SESSION['password'])) {//logged in
							echo '
							<li class="nav_links">
							<button type="button" class="btn loginbtn" id="accBtn" data-toggle="modal" data-target="#accountModal"><i class="fa fa-user fa-fw"></i>Acount</button>
							</li>
							';
						} else {//not logged in
							echo '
							<li class="nav_links">
							<button type="button" class="btn loginbtn" id="myBtn" data-toggle="modal" data-target="#loginModal"><i class="fa fa-user fa-fw"></i> Login</button>
							</li>
							';
						}
					?>
				</ul>
			</div>
		</div>
	</nav>

        <header id="intro">
        	<!-- Carousel -->
        	<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
        		<!-- Indicators -->
        		<ol class="carousel-indicators">
        			<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
        			<li data-target="#carousel-example-generic" data-slide-to="1"></li>
        			<li data-target="#carousel-example-generic" data-slide-to="2"></li>
        		</ol>
        		<!-- Wrapper for slides -->
        		<div class="carousel-inner">
        			<div class="item active">
        				<img src="images/slide_menu1.jpg" alt="First slide">
        				<!-- Static Header -->
        				<div class="header-text hidden-xs">
        					<div class="col-md-12 text-center">
        						<h2>Welcome to Pizza Man</h2>
        						<br>
        						<h3>The right man for the job</h3>
        						<br>
        						<div class="">
        							<a class="btn btn-1 btn-sm" href="menu.php">Our menu</a>
        						</div>
        					</div>
        				</div><!-- /header-text -->
        			</div>
        			<div class="item">
        				<img src="images/slide_menu2.jpg" alt="Second slide">
        				<!-- Static Header -->
        				<div class="header-text hidden-xs">
        					<div class="col-md-12 text-center">
        						<h2>Rated no. 1 restaurant in the world</h2>
        						<br>
        						<h3>We are simply the best, 100% guaranteed</h3>
        						<br>
        						<div class="">
        							<a class="btn btn-1 btn-sm" href="menu.php">Our menu</a>
        						</div>
        					</div>
        				</div><!-- /header-text -->
        			</div>
        			<div class="item">
        				<img src="images/slide_menu3.jpg" alt="Third slide">
        				<!-- Static Header -->
        				<div class="header-text hidden-xs">
        					<div class="col-md-12 text-center">
        						<h2>We have pizza, sides, drinks and everything else imaginable</h2>
        						<br>
        						<h3>We are simply the best</h3>
        						<br>
        						<div class="">
        							<a class="btn btn-1 btn-sm" href="menu.php">Our menu</a>
        						</div>
        					</div>
        				</div><!-- /header-text -->
        			</div>
        		</div>
        		<!-- Controls -->
        		<a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
        			<span class="glyphicon glyphicon-chevron-left"></span>
        		</a>
        		<a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
        			<span class="glyphicon glyphicon-chevron-right"></span>
        		</a>
        	</div><!-- /carousel -->
        </header>
        <!-- Header -->

        <a id='backTop'>Back To Top</a>
        <!-- /Back To Top -->

        <!-- /////////////////////////////////////////Content -->
        <div id="page-content" class="index-page">

        	<!-- ////////////Content Box 01 -->
        	<section class="box-content box-1">
        		<div class="container">
        			<div class="row">
        				<div class="col-sm-4 ">
        					<div class="service">
        						<a href="#"><img src="images/icon1.png" title="icon-name"></a>
        						<h3>Bookings</h3>
        						<p>With Pizza Man, you can reserve a table or seat for you or your family if you wish to come in early and reserve your seats.</p>
        						<a class="btn btn-2 btn-sm" href="#booking">Bookings</a>
        					</div>
        				</div>
        				<div class="col-sm-4 ">
        					<div class="service">
        						<a href="#"><img src="images/icon3.png" title="icon-name"></a>
        						<h3>Map</h3>
        						<p>See how close our store is to your home. Click here and browse the map to see the driving directions to Pizza Man wherever you are!</p>
        						<a class="btn btn-2 btn-sm" href="#location">Map</a>
        					</div>
        				</div>
        				<div class="col-sm-4 ">
        					<div class="service">
        						<a href="#"><img src="images/icon2.png" title="icon-name"></a>
        						<h3>Food Menu</h3>
        						<p>We at Pizza Man take pride in our world class menu favoured by customers and critics alike around the world. Take a look for yourself.</p>
        						<a class="btn btn-2 btn-sm" href="menu.php">Menu</a>
        					</div>
        				</div>
        			</div>
        		</div>
        	</section>

        	<!-- ////////////Content Box 03 -->
        	<section class="box-content box-2 box-style" id="location">
        		<div class="container">
        			<div class="row heading">
        				<h2>Store Location</h2>
        			</div>
        			<div class="col-xs-6">
        				<div id="map">
        					<script>
        						function initMap() {
        							var brisbane = {lat: -27.4698, lng: 153.0251};
        							map = new google.maps.Map(document.getElementById('map'), {
        								zoom: 11,
        								center: brisbane
        							});
        							var marker = new google.maps.Marker({
        								position: brisbane,
        								map: map,
									//icon: "images/logo_new_small.png"
								});
        							
        							var cityCircle = new google.maps.Circle({
        								strokeColor: '#FF0000',
        								strokeOpacity: 0.8,
        								strokeWeight: 2,
        								fillColor: '#FF0000',
        								fillOpacity: 0.35,
        								map: map,
        								center: {lat: -27.4698, lng: 153.0251},
        								radius: 10000
        							});
        						}
        						
        						function getLocation() {
        							if (navigator.geolocation) {
        								navigator.geolocation.getCurrentPosition(showPosition);
        							} else { 
        								alert("Geolocation is not supported by this browser.");
        							}
        						}

        						function showPosition(position) {
        							var myloc = {lat: position.coords.latitude, lng:position.coords.longitude}
        							marker = new google.maps.Marker({
        								position: myloc,
        								map: map
        							});
        							map.panTo(myloc);
        						}
        					</script>
        					<script async defer
								src="https://maps.googleapis.com/maps/api/js?&callback=initMap">
								<!-- API key: key=AIzaSyCOVSlnemkFmmYJbIR7WsDof68FgdsBp5g -->
							</script>
						</div>
					</div>
        		<div class="row">
        			<div class="col-xs-6">
        				<h3>Located in the center of the Brisbane CBD</h3>
        				<p>Our Brisbane headquaters is located right where you want it should be. Straight in the middle of the city,
        				the same distance for everyone in the city.</p><br>
        				<p>We can deliver your food right to your doorstep, as long as you live within our city radius.
        				Click the button below to see if you qualify for a delivery.</p>
        				<a class="btn btn-2 btn-sm" onclick="getLocation()">Find my location</a>
        			</div>
        		</div>
        	</div>
        </section>

        <!-- ////////////Content Box 05 -->
        <section class="box-content box-3" id="booking">
        	<div class="container">
        		<div class="row heading">
        			<div class="col-lg-12">	
        				<h2>Booking</h2>
        				<div class="intro">At Pizza Man, you can reserve a table or seat for you or your family if you wish to come in early and reserve your seats.
        					<p>Just fill in your details in below and we'll take care of the rest.</p>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4 box-item">
						<div class="row">
							<h3>Complete the Submission Form</h3>
							<p>Fill in your details here and we'll send a confirmation email with the details of your booking.</p>
							<h3>Or Just Make a Call</h3>
							<p>Phone: 1800 74992 626 <br>
							Fax: 1800 74992 329</p>
							<p>Email: pizzamanhq@pizzaman.com</p>
						</div>
					</div>
					<div class="col-md-8">
						<form name="form1" method="post" action="mail_booking.php">
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<input type="text" class="form-control input-lg" name="name" id="nameBooking" placeholder="Enter name" required="required" />
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<input type="email" class="form-control input-lg" name="email" id="emailBooking" placeholder="Enter email" required="required" />
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<input type="text" class="form-control input-lg" name="table" id="table" placeholder="Table No." required="required" />
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group">
										<input type="date" class="form-control input-lg" name="date" id="date" placeholder="Date" required="required" />
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group">
										<input type="time" class="form-control input-lg" name="time" id="time" placeholder="Time" required="required" />
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<textarea name="message" id="messageBooking" class="form-control" rows="4" cols="25" required="required"
										placeholder="Any additional info"></textarea>
									</div>
									<button type="submit" class="btn btn-4 btn-block" name="btnBooking" id="submitBoking">Book Now</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</section>
	</div>


        <section class="box-content box-2 box-style" id="contact">
        	<div class="container">
        		<div class="row heading">
        			<div class="col-lg-12">
        				<h2>Contact Us</h2>
        			</div>
        		</div>
        		<div class="row">
        			<div class="col-md-4">
        				<div class="row">
        					<h3>Contact Details</h3>
        					<p>Contact us using any of the methods below or alternatively fill in the form and we'll get
        					back to you as soon as possible.</p><br>
        					<p>Phone: 1800 74992 626</p>
        					<p>Fax: 1800 74992 329</p>
        					<p>Email: pizzamanhq@pizzaman.com</p>
        					<p>Address: 123 Fake St, Brisbane QLD</p>
        				</div>
        			</div>
        			<div class="col-md-8">
        				<form name="form1" method="post" action="mail_contact.php">
        					<div class="row">
        						<div class="col-md-6">
        							<div class="form-group">
        								<input type="text" class="form-control input-lg" name="name" id="name" placeholder="Enter name" required="required" />
        							</div>
        						</div>
        						<div class="col-md-6">
        							<div class="form-group">
        								<input type="email" class="form-control input-lg" name="email" id="email" placeholder="Enter email" required="required" />
        							</div>
        						</div>
        					</div>
        					<div class="row">
        					</div>
        					<div class="row">
        						<div class="col-md-12">
        							<div class="form-group">
        								<textarea name="message" id="message" class="form-control" rows="4" cols="25" required="required"
        								placeholder="Any additional info"></textarea>
        							</div>
        							<button type="submit" class="btn btn-4 btn-block" name="btnBooking" id="btnBbooking">Send Now</button>
        						</div>
        					</div>
        				</form>
        			</div>
        		</div>
        	</div>
        </section>


        <!--Footer-->
        <footer class="page-footer font-small unique-color-dark pt-0" style="background-color: #444444;">
        	<br>

        	<!--Footer Links-->
        	<div class="container mt-5 mb-4 text-center text-md-left">
        		<div class="row mt-3">

        			<!--First column-->
        			<div class="col-md-3 col-lg-4 col-xl-3 mb-4">
        				<h6 class="text-uppercase font-weight-bold"><strong>Sign up for our newsletter!</strong></h6>
        				<hr class="deep-purple accent-2 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
        				<p>Sign up for our newsletter for the latest news on specials and new food available</p>
        				<form class="input-group" id="newsletter" action="mail_newsletter.php">
        					<input name="email" type="text" class="form-control" placeholder="Enter email address" required="required">
        					<button type="submit" class="btn btn-2" type="button">Subscribe!</button>
        				</form>
        			</div>

        			<!--Second column-->
        			<div class="col-md-2 col-lg-2 col-xl-2 mx-auto mb-4">
        				<h6 class="text-uppercase font-weight-bold"><strong>Products</strong></h6>
        				<hr class="deep-purple accent-2 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
        				<p><a href="menu.php">Pizzas</a></p>
        				<p><a href="menu.php#sides">Sides</a></p>
        				<p><a href="menu.php#drinks">Drinks</a></p>
        			</div>

        			<!--Third column-->
        			<div class="col-md-3 col-lg-2 col-xl-2 mx-auto mb-4">
        				<h6 class="text-uppercase font-weight-bold"><strong>Useful links</strong></h6>
        				<hr class="deep-purple accent-2 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
        				<p><a href="index.php#location">Store Location</a></p>
        				<p><a href="index.php#contact">Contact us</a></p>
        				<p><a href="faq.php">FAQ</a></p>
        			</div>

        			<!--Fourth column-->
        			<div class="col-md-4 col-lg-3 col-xl-3">
        				<h6 class="text-uppercase font-weight-bold"><strong>Contact</strong></h6>
        				<hr class="deep-purple accent-2 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
        				<p><i class="fa fa-home mr-3"></i> 123 Fake St, Brisbane QLD</p>
        				<p><i class="fa fa-envelope mr-3"></i> pizzmanhq@pizzaman.com</p>
        				<p><i class="fa fa-phone mr-3"></i> 1800 PIZZA MAN</p>
        				<p><i class="fa fa-print mr-3"></i> + 1800 FAX PIZZA</p>
        			</div>
        		</div>
        	</div>
        </footer>


        <div class="coppy-right">
        	<div class="wrap-footer">
        		<div class="clearfix">
        			<div class="col-md-6 col-md-offset-3">
        				<p>&copy 2018 Pizza Man. All Rights Reserved.</p>
        			</div>
        		</div>
        	</div>
        </div>
        <!-- Footer -->

        <!-- Core JavaScript Files -->
        <script src="js/bootstrap.min.js"></script>
        <script src="js/jquery.backTop.min.js"></script>
        <script>
        	$(document).ready( function() {
        		$('#backTop').backTop({
        			'position' : 1200,
        			'speed' : 500,
        			'color' : 'red',
        		});
        	});
        </script>
        <script src="js/jquery.contact-buttons.js"></script>
        <script src="js/demo.js"></script>
        <script src="js/login_modal.js"></script>
    </body>
    </html>
