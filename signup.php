<?php
/* From: https://www.tutorialrepublic.com/php-tutorial/php-mysql-insert-query.php */
include 'connectMySQL.php';
include 'error.php';

$db = new MySQLDatabase();
$db -> connect();

// Escape user inputs for security
$first_name = mysqli_real_escape_string($db->link, $_REQUEST['first_name']);
$last_name = mysqli_real_escape_string($db->link, $_REQUEST['last_name']);
$email = mysqli_real_escape_string($db->link, $_REQUEST['email']);
$password = mysqli_real_escape_string($db->link, $_REQUEST['password']);
$address = mysqli_real_escape_string($db->link, $_REQUEST['home_address']);
//encrypt password
$password_hashed = password_hash($password, PASSWORD_DEFAULT);

$sql = "SELECT * FROM `users` WHERE email = '$email'";
$result = mysqli_query($db->link, $sql);

if (mysqli_fetch_array($result)) {
	echo "Email already exists";
} else {
	$sql = "INSERT INTO users (first_name, last_name, email, password, address) 
	VALUES ('$first_name', '$last_name', '$email', '$password_hashed', '$address')";
	if(mysqli_query($db->link, $sql)){
		echo "Records added successfully.";
	} else{
		echo "ERROR: Could not able to execute $sql." . mysqli_error($db);
	}
}

// close connection
$db -> disconnect();

?>