<!DOCTYPE html>
<html lang="en">
<!-- Website template from http://freemiumdownload.com/demo?theme=bootstrap-red-restaurant -->
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta name="description" content="Free Bootstrap Themes designed by Zerotheme.com" />
	<meta name="author" content="www.Zerotheme.com" />
	<link rel="icon" href="images/logo_new.png"/>
	<title>Pizza Man</title>
	
	<!-- Bootstrap Core CSS -->
	<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
	<link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	
	<!-- Custom Theme files -->
	<link href="css/style.css" rel="stylesheet" type="text/css"/>
	<link href="css/popuo-box.css" rel="stylesheet" type="text/css" media="all"/>
	<link href="css/contact-buttons.css" rel="stylesheet" type="text/css"/>
	
	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script src="js/jquery.min.js"></script>
	
	<!---pop-up-box---->
	<script type="text/javascript" src="js/modernizr.custom.min.js"></script>    
	<script src="js/jquery.magnific-popup.js" type="text/javascript"></script>
	
	<!---//pop-up-box---->
	<script>
		$(document).ready(function() {
			$('.popup-with-zoom-anim').magnificPopup({
				type: 'inline',
				fixedContentPos: false,
				fixedBgPos: true,
				overflowY: 'auto',
				closeBtnInside: true,
				preloader: false,
				midClick: true,
				removalDelay: 300,
				mainClass: 'my-mfp-zoom-in'
			});
		});
	</script>

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
    <![endif]-->
</head>

<body>
	<!-- Modal templates from https://www.w3schools.com/bootstrap/bootstrap_ref_js_modal.asp -->
	<!-- Sign up modal -->
	<div class="modal fade" id="signModal" role="dialog">
		<div class="modal-dialog">
			
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header" style="padding:35px 50px;">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4><i class="fa fa-lock"></i> Sign Up</h4>
				</div>
				
				<div class="modal-body" style="padding:40px 50px;">
					<form role="form" action="signup.php" method="post">
						<div class="form-group">
							<label for="fname"><i class="fa fa-user"></i> First Name</label>
							<input type="text" class="form-control" id="usrname" name="first_name" placeholder="Enter First Name" required="required">
						</div>
						<div class="form-group">
							<label for="lname"><i class="fa fa-user"></i> Last Name</label>
							<input type="text" class="form-control" id="usrname" name="last_name" placeholder="Enter Last Name" required="required">
						</div>
						<div class="form-group">
							<label for="email"><i class="fa fa-envelope"></i> Email Address</label>
							<input type="email" class="form-control" id="usrname" name="email" placeholder="Enter email" required="required">
						</div>
						<div class="form-group">
							<label for="psw"><i class="fa fa-key"></i> Password</label>
							<input type="password" class="form-control" id="psw" name="password" placeholder="Enter password" required="required">
						</div>
						<div class="form-group">
							<label for="address"><i class="fa fa-map-marker"></i> Home Address</label>
							<input type="text" class="form-control" id="usrname" name="home_address" placeholder="Enter Address">
						</div>
						<div class="form-group">
							<label for="profile_pic"><i class="fa fa-picture-o"></i> Profile Picture</label>
							<input id="file" name="profile_pic" class="file" type="file" accept="image/*">
						</div>
						<div class="checkbox">
							<label><input type="checkbox" value="" checked>Remember me</label>
						</div>
						<button type="submit" class="btn btn-danger btn-block"><i class="fas fa-sign-in-alt"></i> Create Account</button>
					</form>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-danger btn-default pull-left" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancel</button>
				</div>
			</div>
		</div>
	</div>
	
	
	<!-- Forgot password Modal -->
	<div class="modal fade" id="resetModal" role="dialog">
		<div class="modal-dialog">
			
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header" style="padding:35px 50px;">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4><i class="fa fa-lock"></i> Reset Password</h4>
				</div>
				
				<div class="modal-body" style="padding:40px 50px;">
					<p>Enter your email address and an email will be sent to reset your password.</p>
					<form role="form" action="forgot_password.php" method="POST">
						<div class="form-group">
							<label for="usrname"><i class="fa fa-envelope"></i> Email Address</label>
							<input name="email" type="email" class="form-control" id="email" placeholder="Enter email" required="required">
						</div>
						<button type="submit" class="btn btn-danger btn-block"><i class="fas fa-sign-in-alt"></i> Reset Password</button>
					</form>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-danger btn-default pull-left" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancel</button>
				</div>
			</div>
		</div>
	</div>
	
	
	<!-- Login Modal -->
	<div class="modal fade" id="myModal" role="dialog">
		<div class="modal-dialog">
			
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header" style="padding:35px 50px;">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4><i class="fa fa-lock"></i> Login</h4>
				</div>
				
				<div class="modal-body" style="padding:40px 50px;">
					<form role="form" action="login.php" method="POST">
						<input type="hidden" name="saveCookie" value="">
						<div class="form-group">
							<label for="usrname"><i class="fa fa-envelope"></i> Email Address</label>
							<input type="email" name="email" class="form-control" id="usrname" placeholder="Enter email" required="required">
						</div>
						<div class="form-group">
							<label for="psw"><i class="fa fa-key"></i> Password</label>
							<input type="password" name="password" class="form-control" id="psw" placeholder="Enter password" required="required">
						</div>
						<div class="checkbox">
							<label><input type="checkbox" value="" checked>Remember me</label>
						</div>
						<button type="submit" class="btn btn-danger btn-block"><i class="fas fa-sign-in-alt"></i> Login</button>
					</form>
				</div>
				
				<div class="modal-footer">
					<button type="submit" class="btn btn-danger btn-default pull-left" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancel</button>
					<p>Not a member? <button type="button" class="btn signupbtn" id="myBtn2" data-toggle="signModal">Sign Up</button></p>
					<button type="button" class="btn signupbtn" id="myBtn3" data-toggle="resetModal">Forgot your password?</button>
				</div>
			</div>
		</div>
	</div>
	
	
	<!-- ///////////////Navigation -->
	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
				data-target="#bs-example-navbar-collapse-1">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>

			</button>
			<a class="navbar-brand" href="index.php">
				<img src="images/logo_new.png" class="hidden-xs" alt="Logo">
				<h3 class="visible-xs">Pizza Man</h3>
			</a>
			
		</div>
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav navbar-right">
				<li class="nav_links">
					<a class="page-scroll" href="menu.php"><i class="fa fa-cutlery fa-fw"></i> Menu</a>
				</li>
				<li class="nav_links">
					<a class="page-scroll" href="index.php#location"><i class="fa fa-map-marker fa-fw"></i> Store Location</a>
				</li>
				<li class="nav_links">
					<a class="page-scroll" href="index.php#booking"><i class="fa fa-book fa-fw"></i> Booking</a>
				</li>
				<li class="nav_links">
					<a class="page-scroll" href="index.php#contact"><i class="fa fa-phone fa-fw"></i> Contact Us</a>
				</li>
				<li class="nav_links">
					<button type="button" class="btn loginbtn" id="myBtn" data-toggle="myModal"><i class="fa fa-user fa-fw"></i> Login</button>
				</li>
				<li class="nav_links">
					<a class="page-scroll" href="cart.php"><i class="fa fa-shopping-cart fa-fw"></i> Cart</a>
				</li>
			</ul>
		</div>
	</div>
</nav>

<a id='backTop'>Back To Top</a>
<!-- /Back To Top -->


<section class="box-content box-2 box-style" id="cart"><br>
	<div class="container">
		<div class="heading">
			<div class="col-lg-12">	
				<h2>Frequently Asked Questions</h2>
			</div>
		</div><br><br>
		<div>
			<h3>1. DO YOU HAVE GLUTEN FREE PIZZA OPTIONS?</h3>
			<p>We unfortunetly do not have any gluten free pizza options. We pride ourselves on having 100% gluten pizzas.</p>
			<br>
			<h3>2. WHY CAN'T YOU DELIVER TO MY ADDRESS?</h3>
			<p>Unfortunetly due to the high demand, we can only deliver pizzas within a certain radius around our central Brisbane store. <a href="index.html#location">Click here</a> for more details.</p>
			<br>
			<h3>3. WHAT BROWSERS IS SUPPORTED ON THIS SITE?</h3>
			<p>At this point in time, the website is supported for devices running Google Chrome v.64 or greater or Mozilla Firefox v.56 or greater.</p>
			<br>
			<h3>4. HOW CAN I PAY FOR MY PIZZA?</h3>
			<p>You have the option to either pay in store or pay via credit or debit card when you order.</p>
			<br>
			<h3>5. DO YOU HAVE ANY SPECIALS?</h3>
			<p>Sign up for our newsletter for the latest specials and offers.</p>
			<br>
			<h3>6. WHERE IS MY INFORMATION STORED?</h3>
			<p>Your information is stored on the Pizza Man databases which are safe and secure.</p>
			<br>
			<h3>7. CAN I PAY BY CHEQUE?</h3>
			<p>Pizza Man does not currently accept payment by cheque. We only accept cash in-store or credit/debit cards.</p>
		</div>
	</div>
</section>



	<!--Footer-->
        <footer class="page-footer font-small unique-color-dark pt-0" style="background-color: #444444;">
        	<br>

        	<!--Footer Links-->
        	<div class="container mt-5 mb-4 text-center text-md-left">
        		<div class="row mt-3">

        			<!--First column-->
        			<div class="col-md-3 col-lg-4 col-xl-3 mb-4">
        				<h6 class="text-uppercase font-weight-bold"><strong>Sign up for our newsletter!</strong></h6>
        				<hr class="deep-purple accent-2 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
        				<p>Sign up for our newsletter for the latest news on specials and new food available</p>
        				<form class="input-group" id="newsletter" action="mail_newsletter.php">
        					<input name="email" type="text" class="form-control" placeholder="Enter email address" required="required">
        					<button type="submit" class="btn btn-2" type="button">Subscribe!</button>
        				</form>
        			</div>

        			<!--Second column-->
        			<div class="col-md-2 col-lg-2 col-xl-2 mx-auto mb-4">
        				<h6 class="text-uppercase font-weight-bold"><strong>Products</strong></h6>
        				<hr class="deep-purple accent-2 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
        				<p><a href="menu.php">Pizzas</a></p>
        				<p><a href="menu.php#sides">Sides</a></p>
        				<p><a href="menu.php#drinks">Drinks</a></p>
        			</div>

        			<!--Third column-->
        			<div class="col-md-3 col-lg-2 col-xl-2 mx-auto mb-4">
        				<h6 class="text-uppercase font-weight-bold"><strong>Useful links</strong></h6>
        				<hr class="deep-purple accent-2 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
        				<p><a href="index.php#location">Store Location</a></p>
        				<p><a href="index.php#contact">Contact us</a></p>
        				<p><a href="faq.php">FAQ</a></p>
        			</div>

        			<!--Fourth column-->
        			<div class="col-md-4 col-lg-3 col-xl-3">
        				<h6 class="text-uppercase font-weight-bold"><strong>Contact</strong></h6>
        				<hr class="deep-purple accent-2 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
        				<p><i class="fa fa-home mr-3"></i> 123 Fake St, Brisbane QLD</p>
        				<p><i class="fa fa-envelope mr-3"></i> pizzmanhq@pizzaman.com</p>
        				<p><i class="fa fa-phone mr-3"></i> 1800 PIZZA MAN</p>
        				<p><i class="fa fa-print mr-3"></i> + 1800 FAX PIZZA</p>
        			</div>
        		</div>
        	</div>
        </footer>


<div class="coppy-right">
	<div class="wrap-footer">
		<div class="clearfix">
			<div class="col-md-6 col-md-offset-3">
				<p>Pizza Man - Copyright &copy 2018</p>
			</div>
		</div>	
	</div>
</div>
<!-- Footer -->

<!-- Core JavaScript Files -->
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.backTop.min.js"></script>
<script>
	$(document).ready( function() {
		$('#backTop').backTop({
			'position' : 1200,
			'speed' : 500,
			'color' : 'red',
		});
	});
</script>
<script src="js/jquery.contact-buttons.js"></script>
<script src="js/demo.js"></script>
<script src="js/login_modal.js"></script>
</body>
</html>

