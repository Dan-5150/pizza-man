<!DOCTYPE html>
<?php
include 'connectMySQL.php';
include 'error.php';
session_start();
?>
<html lang="en">
<!-- Website template from http://freemiumdownload.com/demo?theme=bootstrap-red-restaurant -->
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta name="description" content="Free Bootstrap Themes designed by Zerotheme.com" />
	<meta name="author" content="www.Zerotheme.com" />
	<link rel="icon" href="images/logo_new.png"/>
	<title>Menu - Pizza Man</title>
	
	<!-- Bootstrap Core CSS -->
	<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
	<link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	
	<!-- Custom Theme files -->
	<link href="css/style.css" rel="stylesheet" type="text/css"/>
	<link href="css/popuo-box.css" rel="stylesheet" type="text/css" media="all"/>
	<link href="css/contact-buttons.css" rel="stylesheet" type="text/css"/>
	
	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script src="js/jquery.min.js"></script>
	
	<!---pop-up-box---->
	<script type="text/javascript" src="js/modernizr.custom.min.js"></script>    
	<script src="js/jquery.magnific-popup.js" type="text/javascript"></script>
	
	<!---//pop-up-box---->
	<script>
		$(document).ready(function() {
			$('.popup-with-zoom-anim').magnificPopup({
				type: 'inline',
				fixedContentPos: false,
				fixedBgPos: true,
				overflowY: 'auto',
				closeBtnInside: true,
				preloader: false,
				midClick: true,
				removalDelay: 300,
				mainClass: 'my-mfp-zoom-in'
			});
		});
	</script>

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
    <![endif]-->
</head>

<body>
	<!-- Modal templates from https://www.w3schools.com/bootstrap/bootstrap_ref_js_modal.asp -->
	<!-- Login Modal -->
	<div class="modal fade" id="loginModal" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header" style="padding:35px 50px;">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4><i class="fa fa-lock"></i> Login</h4>
				</div>

				<div class="modal-body" style="padding:40px 50px;">
					<form role="form" id="login" method="POST">
						<div class="form-group">
							<label for="usrname"><i class="fa fa-user"></i>Email Address</label>
							<input type="email" name="email" class="form-control" id="emailLogin" placeholder="Enter email" required="required">
						</div>
						<div class="form-group">
							<label for="psw"><i class="fa fa-key"></i>Password</label>
							<input type="password" name="password" class="form-control" id="passwordLogin" placeholder="Enter password" required="required">
						</div>
						<!-- <div class="checkbox">
						  <label><input type="checkbox" value="" checked>Remember me</label>
						</div> -->
						<button type="submit" class="btn btn-danger btn-block"><i class="fas fa-sign-in-alt"></i>Login</button>
					</form>

					<script type="text/javascript">
						var form = document.getElementById("login");
						form.addEventListener("submit", function (event) {
							event.preventDefault();
							login();
						});
						function login() {
							var data = new FormData();
							var email = document.getElementById("emailLogin").value;
							var password = document.getElementById("passwordLogin").value;
							data.append('email', email);
							data.append('password', password);
							var xhttp  = new XMLHttpRequest();
							xhttp.onreadystatechange = function() {
								if (this.readyState == 4 && this.status == 200) {
									if(this.responseText=="Login Successful"){
										window.alert("Login Successful");
										location.reload();
									} else if (this.responseText=="Invalid Password") {
										window.alert("Invalid Password");
									} else if (this.responseText=="Invalid Email"){
										window.alert("Invalid Email");
									}
								}
							};
							xhttp.open("POST", "login.php", true);
							xhttp.send(data);
						}
					</script>

				</div>

				<div class="modal-footer">
					<button type="submit" class="btn btn-danger btn-default pull-left" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancel</button>
					<p>Not a member? <button type="button" class="btn signupbtn" id="myBtn2" data-toggle="signModal">Sign Up</button></p>
					<button type="button" class="btn signupbtn" id="myBtn3" data-toggle="resetModal">Forgot your password?</button>
				</div>
			</div>
		</div>
	</div>

	<!-- Sign up modal -->
	<div class="modal fade" id="signModal" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header" style="padding:35px 50px;">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4><i class="fa fa-lock"></i> Sign Up</h4>
				</div>

				<div class="modal-body" style="padding:40px 50px;">
					<form role="form" id="signup" method="POST">
						<div class="form-group">
							<label for="fname"><i class="fa fa-user"></i> First Name</label>
							<input type="text" class="form-control" id="fnameSignup" name="first_name" placeholder="Enter First Name" required="required">
						</div>
						<div class="form-group">
							<label for="lname"><i class="fa fa-user"></i> Last Name</label>
							<input type="text" class="form-control" id="lnameSignup" name="last_name" placeholder="Enter Last Name" required="required">
						</div>
						<div class="form-group">
							<label for="email"><i class="fa fa-envelope"></i> Email Address</label>
							<input type="email" class="form-control" id="emailSignup" name="email" placeholder="Enter email" required="required">
						</div>
						<div class="form-group">
							<label for="psw"><i class="fa fa-key"></i> Password</label>
							<input type="password" class="form-control" id="passwordSignup" name="password" placeholder="Enter password" required="required">
						</div>
						<div class="form-group">
							<label for="address"><i class="fa fa-map-marker"></i> Home Address</label>
							<input type="text" class="form-control" id="addressSignup" name="home_address" placeholder="Enter Address">
						</div>
					<!-- <div class="checkbox">
					  <label><input type="checkbox" value="" checked>Remember me</label>
					</div> -->
					<button type="submit" class="btn btn-danger btn-block"><i class="fas fa-sign-in-alt"></i> Create Account</button>
				</form>

				<script type="text/javascript">
					var form = document.getElementById("signup");
					form.addEventListener("submit", function (event) {
						event.preventDefault();
						signup();
					});
					function signup() {
						var data = new FormData();
						var fname = document.getElementById("fnameSignup").value;
						var lname = document.getElementById("lnameSignup").value;
						var email = document.getElementById("emailSignup").value;
						var password = document.getElementById("passwordSignup").value;
						var address = document.getElementById("addressSignup").value;
						data.append('first_name', fname);
						data.append('last_name', lname);
						data.append('email', email);
						data.append('password', password);
						data.append('home_address', address);
						var xhttp  = new XMLHttpRequest();
						xhttp.onreadystatechange = function() {
							if (this.readyState == 4 && this.status == 200) {
								if(this.responseText=="Records added successfully."){
									window.alert("Sign Up Successful");
									jQuery("#signModal").modal('hide');
								} else if (this.responseText=="Email already exists") {
									window.alert("Email already exists");
								} else {
									window.alert(this.responseText);
								}
							}
						};
						xhttp.open("POST", "signup.php", true);
						xhttp.send(data);
					}
				</script>

			</div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-danger btn-default pull-left" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancel</button>
			</div>
		</div>
	</div>
</div>

<!-- Forgot password Modal -->
<div class="modal fade" id="resetModal" role="dialog">
	<div class="modal-dialog">
		
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header" style="padding:35px 50px;">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4><i class="fa fa-lock"></i> Reset Password</h4>
			</div>
			
			<div class="modal-body" style="padding:40px 50px;">
				<p>Enter your email address and an email will be sent to reset your password.</p>
				<form role="form" action="forgot_password.php" method="POST">
					<div class="form-group">
						<label for="usrname"><i class="fa fa-envelope"></i> Email Address</label>
						<input name="email" class="form-control" id="emailReset" placeholder="Enter email" required="required">
					</div>
					<button type="submit" class="btn btn-danger btn-block"><i class="fas fa-sign-in-alt"></i> Reset Password</button>
				</form>
			</div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-danger btn-default pull-left" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancel</button>
			</div>
		</div>
	</div>
</div>

<!--Account Modal-->
<div class="modal fade" id="accountModal" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header" style="padding:35px 50px;">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4><i class="fa fa-lock"></i>Account</h4>
			</div>
			<div class="modal-body" style="padding:40px 50px;">
				<button onclick="logout()" type="button" class="btn btn-danger" id="logoutBtn"><i class="fa fa-user fa-fw"></i>Logout</button>
				<button type="button" class="btn btn-primary" id="accBtn" data-toggle="modal" data-target="#accountModal2"><i class="fa fa-user fa-fw"></i>Modify Account</button>
			</div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-danger btn-default pull-left" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancel</button>
			</div>
			<script type="text/javascript">
				
				
				function logout(){
					var xmlhttp = new XMLHttpRequest();
					xmlhttp.onreadystatechange = function() {
							if (this.readyState == 4 && this.status == 200) {
								window.alert("Logged out");
								location.reload();
							}
						};
					xmlhttp.open("GET", "unset.php", true);
					xmlhttp.send();
				}
			</script>
		</div>
	</div>
</div>

<!--Account Modal 2-->
<div class="modal fade" id="accountModal2" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header" style="padding:35px 50px;">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4><i class="fa fa-lock"></i>Modify</h4>
			</div>
			<div class="modal-body" style="padding:40px 50px;">
				<form role="form" id="change">
					<div class="form-group">
						<label for="newPassword"><i class="fa fa-envelope"></i>New Password</label>
						<input name="password" class="form-control" id="passwordNew" placeholder="Enter new password" required="required">
					</div>
					<button type="submit" class="btn btn-danger btn-block"><i class="fas fa-sign-in-alt"></i>Update</button>
				</form>
			</div>
			<div class="modal-body" style="padding:40px 50px;">
				<button onclick="deleteAccount()" type="button" class="btn btn-danger btn-block" id="deleteBtn"><i class="fa fa-user fa-fw"></i>Delete Account</button>
			</div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-danger" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancel</button>
			</div>
			<script type="text/javascript">
				var form = document.getElementById("change");
					form.addEventListener("submit", function (event) {
						event.preventDefault();
						change();
					});
				function change(){
					var data = new FormData();
					var newpass = document.getElementById("passwordNew").value;
					data.append('email', email);
					data.append('password', newpass);
					var xmlhttp = new XMLHttpRequest();
					xmlhttp.onreadystatechange = function() {
						if (this.readyState == 4 && this.status == 200) {
							if (this.responseText=="Password changed") {
								window.alert("Password changed");
								jQuery("#accountModal2").modal('hide');
							} else {
								window.alert(this.responseText);
							}
						}
					};
					xmlhttp.open("POST", "changepass.php", true);
					xmlhttp.send(data);
				}
				
				function deleteAccount(){
					var data = new FormData();
					data.append('email', email);
					var xmlhttp = new XMLHttpRequest();
					xmlhttp.onreadystatechange = function() {
						if (this.readyState == 4 && this.status == 200) {
							if (this.responseText=="Account deleted") {
								logout();
								window.alert("Account deleted");
								location.reload();
							} else {
								window.alert(this.responseText);
							}
						}
					};
					xmlhttp.open("POST", "deleteAccount.php", true);
					xmlhttp.send(data);
				}

			</script>
		</div>
	</div>
</div>

<!-- Navigation -->
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse"
			data-target="#bs-example-navbar-collapse-1">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>

		</button>
		<a class="navbar-brand" href="index.php">
			<img src="images/logo_new.png" class="hidden-xs" alt="Logo">
			<h3 class="visible-xs">Pizza Man</h3>
		</a>

	</div>
	<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		<ul class="nav navbar-nav navbar-right">
			<li class="nav_links">
				<a class="page-scroll" href="menu.php"><i class="fa fa-cutlery fa-fw"></i> Menu</a>
			</li>
			<li class="nav_links">
				<a class="page-scroll" href="index.php#location"><i class="fa fa-map-marker fa-fw"></i> Store Location</a>
			</li>
			<li class="nav_links">
				<a class="page-scroll" href="index.php#booking"><i class="fa fa-book fa-fw"></i> Booking</a>
			</li>
			<li class="nav_links">
				<a class="page-scroll" href="index.php#contact"><i class="fa fa-phone fa-fw"></i> Contact Us</a>
			</li>
			<?php
                    	if (isset($_SESSION['email']) && isset($_SESSION['password'])) {//logged in
                    		echo '
							<li class="nav_links">
                    		<button type="button" class="btn loginbtn" id="accBtn" data-toggle="modal" data-target="#accountModal"><i class="fa fa-user fa-fw"></i>Acount</button>
                    		</li>
                    		';
                    	} else {//not logged in
                    		echo '
                    		<li class="nav_links">
                    		<button type="button" class="btn loginbtn" id="myBtn" data-toggle="modal" data-target="#loginModal"><i class="fa fa-user fa-fw"></i> Login</button>
                    		</li>
							';
                    	}
                    	?>
                    </ul>
                </div>
            </div>
        </nav>
        
        <a id='backTop'>Back To Top</a>
        <!-- /Back To Top -->
        
        <!-- Content -->


        <section class="box-content box-2 box-style" id="cart"><br>
        	<div class="container">
        		<div class="heading">
        			<div class="col-lg-12">	
        				<h2>Menu</h2>
        				<h6><em>The world famous Pizza Man menu!</em></h6>
        				<h6><em>Feel free to browse the finest food in all the world.</em></h6>
						<button type='submit' id=$name onclick='addToCart(this.id)' class='btn btn-primary' type='button'><i class='fa fa-shopping-cart'></i> Add to cart</button>
        			</div>
        		</div>
			
        		<!-- Pizzas -->

        		<div class="col-lg-12">	
        			<h4>Pizzas</h4>
        		</div>
				
        		<?php
        			$db = new MySQLDatabase();
					$db -> connect();

					$query = "SELECT * FROM items WHERE category = 'pizza'";
					$result = mysqli_query($db->link, $query);

					if($result){
						while ($row = mysqli_fetch_array($result)) {
							$name = $row['name'];
							$price = $row['price'];
							echo "
							<div class='col-xs-6'>
								<div class='container'>
				        			<div class='col-lg-5'>
										<div class='row'>
											<h2>$name</h2>
										</div>
										<div class='row'>
											<img class='img-responsive' src='images/$name.jpg' alt='Pizza Image' width='350' height='350' style='border-radius: 25px'>
										</div>
										<div class='row'>
											<h3>$$price</h3>
										</div>
										<div class='row'>
											<button type='submit' id=$name onclick='addToCart(this.id)' class='btn btn-primary' type='button'><i class='fa fa-shopping-cart'></i> Add to cart</button></div>
										</div>
									</div>
								</div>
								";
						}
					} else {
						echo "Error";
					}

					$db -> disconnect();
        		?>

        		<!-- Sides -->

        		<div class="col-lg-12" id="sides">	
        			<h4>Sides</h4>
        		</div>

        		<?php
        			$db = new MySQLDatabase();
					$db -> connect();

					$query = "SELECT * FROM items WHERE category = 'side'";
					$result = mysqli_query($db->link, $query);

					if($result){
						while ($row = mysqli_fetch_array($result)) {
							$name = $row['name'];
							$price = $row['price'];
							echo "
							<div class='col-xs-6'>
								<div class='container'>
				        			<div class='col-lg-4'>
										<div class='row'>
											<h2>$name</h2>
										</div>
										<div class='row' >
											<img class='img-responsive' src='images/$name.jpg' alt='Pizza Image' width='350' height='350' style='border-radius: 25px'>
										</div>
										<div class='row'>
											<h3>$$price</h3>
										</div>
										<div class='row'>
											<button type='submit' id=$name onclick='addToCart(this.id)' class='btn btn-primary' type='button'><i class='fa fa-shopping-cart'></i> Add to cart</button></div>
										</div>
									</div>
								</div>
							";
						}
					} else {
						echo "Error";
					}

					$db -> disconnect();
        		?>

        		<!-- Drinks -->

        		<div class="col-lg-12" id="drinks">	
        			<h4>Drinks</h4>
        		</div>
        		<?php
        			$db = new MySQLDatabase();
					$db -> connect();

					$query = "SELECT * FROM items WHERE category = 'drink'";
					$result = mysqli_query($db->link, $query);

					if($result){
						while ($row = mysqli_fetch_array($result)) {
							$name = $row['name'];
							$price = $row['price'];
							echo "
							<div class='col-xs-6'>
								<div class='container'>
				        			<div class='col-lg-4'>
										<div class='row'>
											<h2>$name</h2>
										</div>
										<div class='row'>
											<img class='img-responsive' src='images/$name.jpg' alt='Pizza Image' width='350' height='350' style='border-radius: 25px'>
										</div>
										<div class='row'>
											<h3>$$price</h3>
										</div>
										<div class='row' >
											<button type='submit' id=$name onclick='addToCart(this.id)' class='btn btn-primary' type='button'><i class='fa fa-shopping-cart'></i> Add to cart</button></div>
										</div>
									</div>
								</div>
							";
						}
					} else {
						echo "Error";
					}
					$db -> disconnect();
        		?>

        		<script type="text/javascript">
        			function addToCart(itemName){
        				console.log(itemName);
        			}
        		</script>
		</div>
	</section>
		
        <!--Footer-->
        <footer class="page-footer font-small unique-color-dark pt-0" style="background-color: #444444;">
        	<br>

        	<!--Footer Links-->
        	<div class="container mt-5 mb-4 text-center text-md-left">
        		<div class="row mt-3">

        			<!--First column-->
        			<div class="col-md-3 col-lg-4 col-xl-3 mb-4">
        				<h6 class="text-uppercase font-weight-bold"><strong>Sign up for our newsletter!</strong></h6>
        				<hr class="deep-purple accent-2 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
        				<p>Sign up for our newsletter for the latest news on specials and new food available</p>
        				<form class="input-group" id="newsletter" action="mail_newsletter.php">
        					<input name="email" type="text" class="form-control" placeholder="Enter email address" required="required">
        					<button type="submit" class="btn btn-2" type="button">Subscribe!</button>
        				</form>
        			</div>

        			<!--Second column-->
        			<div class="col-md-2 col-lg-2 col-xl-2 mx-auto mb-4">
        				<h6 class="text-uppercase font-weight-bold"><strong>Products</strong></h6>
        				<hr class="deep-purple accent-2 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
        				<p><a href="menu.php">Pizzas</a></p>
        				<p><a href="menu.php#sides">Sides</a></p>
        				<p><a href="menu.php#drinks">Drinks</a></p>
        			</div>

        			<!--Third column-->
        			<div class="col-md-3 col-lg-2 col-xl-2 mx-auto mb-4">
        				<h6 class="text-uppercase font-weight-bold"><strong>Useful links</strong></h6>
        				<hr class="deep-purple accent-2 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
        				<p><a href="index.php#location">Store Location</a></p>
        				<p><a href="index.php#contact">Contact us</a></p>
        				<p><a href="faq.php">FAQ</a></p>
        			</div>

        			<!--Fourth column-->
        			<div class="col-md-4 col-lg-3 col-xl-3">
        				<h6 class="text-uppercase font-weight-bold"><strong>Contact</strong></h6>
        				<hr class="deep-purple accent-2 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
        				<p><i class="fa fa-home mr-3"></i> 123 Fake St, Brisbane QLD</p>
        				<p><i class="fa fa-envelope mr-3"></i> pizzmanhq@pizzaman.com</p>
        				<p><i class="fa fa-phone mr-3"></i> 1800 PIZZA MAN</p>
        				<p><i class="fa fa-print mr-3"></i> + 1800 FAX PIZZA</p>
        			</div>
        		</div>
        	</div>
        </footer>
        
        
        <div class="coppy-right">
        	<div class="wrap-footer">
        		<div class="clearfix">
        			<div class="col-md-6 col-md-offset-3">
        				<p>Pizza Man - Copyright &copy 2018</p>
        			</div>
        		</div>	
        	</div>
        </div>
        <!-- Footer -->
        
        <!-- Core JavaScript Files -->
        <script src="js/bootstrap.min.js"></script>
        <script src="js/jquery.backTop.min.js"></script>
        <script>
        	$(document).ready( function() {
        		$('#backTop').backTop({
        			'position' : 1200,
        			'speed' : 500,
        			'color' : 'red',
        		});
        	});
        </script>
        <script src="js/jquery.contact-buttons.js"></script>
        <script src="js/demo.js"></script>
        <script src="js/login_modal.js"></script>
    </body>
    </html>
    
